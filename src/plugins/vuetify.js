import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: colors.teal.lighten1,
                secondary: colors.indigo.base,
                accent: colors.cyan.darken1,
                error: colors.pink.base,
                warning: colors.red.base,
                info: colors.lightBlue.base,
                success: colors.green.base
            },
        },
    },
});
