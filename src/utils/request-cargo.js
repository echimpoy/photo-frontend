import axios from 'axios';
import store from '@/store';
import router from '@/router';

const service = axios.create({
  baseURL: process.env.VUE_APP_CARGO_API, // url = base url + request url // use proxy instead
  // withCredentials: true, // send cookies when cross-domain requests
  // timeout: 5000,
  // headers: { 'Content-Type': 'multipart/form-data', 'Accept': 'application/json' },
  Authorization: ''
});

service.interceptors.request.use(
  config => {
    if (store.getters._token) {
      // axios.defaults.headers.common.Authorization = 'Bearer ' + store.getters._token
      if (!config.headers["Authorization"]) {
        config.headers['X-Requested-With'] = 'XMLHttpRequest';
        config.headers['Authorization'] = 'Bearer ' + store.getters._token;
      }
    }
    
    //-- convert null values to void for post requests:
    if (config.method == 'post' && config.data instanceof FormData) {
      for (var key of config.data.keys()) {
        // here you can add filtering conditions
        if (config.data.get(key) == null || config.data.get(key) == "null") { //?wtf null converts to string "null"
          config.data.delete(key); // -> void 0
        }
      }
    }

    return config;
  },
  error => {
    Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  response => {
    return response.data;
  },
  error => {
    // request canceled to start again with other params
    if(error.message == 'OPERATION_CANCELED') {
      return Promise.reject(error);
    }

    console.error('err', error);
    console.error('err responce', error.response);
    if (error.response.status === 401) {
      store.dispatch('app/setError', { text: 'User not authenticated', type: 'error', code: 401 });
      
      if (store.getters._token) { // if user loggedin then logout
        store.dispatch('user/logOut').then(() => {
          router.push('/login');
          // window.location.reload();
        });
      }
    } else {
      if (error.response.data.errors) {
        store.dispatch('app/setError', { text: error.response.statusText || error, type: 'error', errors: error.response.data.errors });
      } else {
        store.dispatch('app/setError', { text: error.response.statusText || error, type: 'error' });
      }
    }
    return Promise.reject(error);
  }
);

export default service;
