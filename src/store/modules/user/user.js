import Vue from 'vue'
import {
  login,
  updateProfile,
  fetchUsers,
  createUser,
  editUser,
  deleteUser
} from '@/api/user';

import { getUser, setUser, logOutUser } from '@/utils/auth';

import {
  SET_USER,
  LOGOUT_USER,
  SET_TOKEN,
  REMOVE_TOKEN,
  FETCH_USERS,
  CREATE_USER,
  EDIT_USER,
  DELETE_USER,
  SET_USER_PAGE,
  SET_USER_TOTAL_PAGES,
  SET_USER_SEARCH,
  SET_USER_STATUS,
  SET_USER_ROLE,
} from '@/store/mutation-types';

const state = () => {
  return {
    user: getUser() || void 0,
    users: [],
    page: 1,
    total_pages: 1,
    // filters:
    search: void 0,
    status: void 0,
    role: void 0,
    per_page: 15,
  };
};

const getters = {
  user: state => {
    return state.user
  },
  isUserLoggedIn: state => {
    return !!state.user
  },
  users: state => {
    return state.users
  },
  page: state => {
    return state.page
  },
  total_pages: state => {
    return state.total_pages
  },
  per_page: state => {
    return state.per_page
  },
  search: state => {
    return state.search
  },
  status: state => {
    return state.status
  },
  role: state => {
    return state.role
  },
};

const mutations = {
  [SET_USER](state, payload) {
    setUser(payload);
    state.user = payload;
  },
  // --
  [LOGOUT_USER](state, payload) {
    logOutUser();
    state.user = payload;
  },
  [FETCH_USERS](state, payload) {
    state.users = payload;
  },
  [CREATE_USER](state, payload) {
    state.users.push(payload);
  },
  [EDIT_USER](state, payload) {
    const index = state.users.findIndex(item => item.id === payload.id);
    Vue.set(state.users, index, payload);
  },
  [DELETE_USER](state, payload) {
    state.users.map((user, index) => {
      if (user.id == payload.id) {
        state.users.splice(index, 1);
      }
    })
  },
  [SET_USER_PAGE](state, payload) {
    state.page = payload;
  },
  [SET_USER_TOTAL_PAGES](state, payload) {
    state.total_pages = payload;
  },
  [SET_USER_SEARCH](state, payload) {
    state.search = payload;
  },
  [SET_USER_STATUS](state, payload) {
    state.status = payload;
  },
  [SET_USER_ROLE](state, payload) {
    state.role = payload;
  },
};

const actions = {
  async logIn({ commit }, userInfo) {
    try {
      const response = await login(userInfo);

      const { success, access_token, user } = response;

      if (!success) {
        console.error(response);
        return Promise.reject(response)
      }

      commit(SET_TOKEN, access_token, {
        root: true
      });
      commit(SET_USER, user);
    } catch (error) {
      commit(REMOVE_TOKEN, '', {
        root: true
      });
      commit(LOGOUT_USER, '');
      console.error(error);
      return Promise.reject(error)
    }
  },

  setUser({ commit }, payload) {
    commit(SET_USER, payload);
  },

  async updateProfile({
    commit
  }, payload) {
    try {
      let {
        data
      } = await updateProfile(payload);
      commit(SET_USER, data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  logOut({ commit /*, state*/ }) {
    return new Promise((resolve/*, reject*/) => {
      commit(REMOVE_TOKEN, '', {
        root: true
      });
      commit(LOGOUT_USER, '');
      resolve();
    });
  },

  resetToken(/*{ commit }*/) {
    return new Promise(resolve => {
      resolve();
    });
  },

  async fetchUsers({
    commit, state
  }, params = {}) {
    try {
      // filters
      params.per_page = state.per_page;
      params.page = state.page;
      params.search = state.search ? state.search : void 0 ;
      params.status = state.status;
      params.role = state.role;

      const response = await fetchUsers(params);

      if (!response.success || !response.data) {
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t load users.')
      }

      commit(SET_USER_TOTAL_PAGES, Math.ceil(response.total / state.per_page));
      commit(FETCH_USERS, response.data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },

  async createNewUser({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await createUser(payload);
      commit(CREATE_USER, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
      return data;
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  async editUser({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await editUser(payload);
      commit(EDIT_USER, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  async deleteUser({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await deleteUser(payload);
      commit(DELETE_USER, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },
  // --
  setPage({
    commit
  }, page) {
    commit(SET_USER_PAGE, page);
  },
  setSearch({
    commit
  }, search) {
    commit(SET_USER_SEARCH, search);
  },
  setStatus({
    commit
  }, status) {
    commit(SET_USER_STATUS, status);
  },
  setRole({
    commit
  }, role) {
    commit(SET_USER_ROLE, role);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
