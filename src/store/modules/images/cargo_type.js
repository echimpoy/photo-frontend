import {
  fetchCargoTypes,
} from '@/api/cargo_type';

import {
  FETCH_CARGO_TYPES,
} from '@/store/mutation-types';

const state = () => {
  return {
    // cargo_type: void 0,
    cargo_types: [],
  };
};

const getters = {
  cargo_types: state => {
    return state.cargo_types
  },
};

const mutations = {
  [FETCH_CARGO_TYPES](state, payload) {
    state.cargo_types = payload;
  },
};

const actions = {
  async fetchCargoTypes({
    commit
  }, params) {
    try {
      const response = await fetchCargoTypes(params);

      if (!response.success || !response.data) {
        console.log(response);
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t load cargo_types.')
      }

      commit(FETCH_CARGO_TYPES, response.data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },

};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
