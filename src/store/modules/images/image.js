import Vue from 'vue'
import {
  fetchImages,
  uloadImages,
  editImage,
  deleteImage
} from '@/api/image';

import {
  FETCH_IMAGES,
  CREATE_IMAGE,
  EDIT_IMAGE,
  DELETE_IMAGE,
  SET_IMAGE_PAGE,
  SET_IMAGE_TOTAL_PAGES,
  SET_IMAGE_SEARCH,
  SET_IMAGE_STATUS,
  SET_IMAGE_TYPE,
  SET_IMAGE_TRANSPORT_TYPES,
  SET_IMAGE_CARGO_TYPES,
  SET_IMAGE_TAGS,
} from '@/store/mutation-types';

const state = () => {
  return {
    // image: void 0,
    images: [],
    page: 1,
    total_pages: 1,
    // filters:
    search: void 0,
    status: void 0,
    type: void 0,
    per_page: 15,
    transport_types: [],
    cargo_types: [],
    tags: [],
  };
};

const getters = {
  images: state => {
    return state.images
  },
  page: state => {
    return state.page
  },
  total_pages: state => {
    return state.total_pages
  },
  per_page: state => {
    return state.per_page
  },
  search: state => {
    return state.search
  },
  status: state => {
    return state.status
  },
  type: state => {
    return state.type
  },
  transport_types: state => {
    return state.transport_types
  },
  cargo_types: state => {
    return state.cargo_types
  },
  tags: state => {
    return state.tags
  },
};

const mutations = {
  [FETCH_IMAGES](state, payload) {
    state.images = payload;
  },
  [CREATE_IMAGE](state, payload) {
    payload.forEach(p => {
      state.images.push(p)
    });
  },
  [EDIT_IMAGE](state, payload) {
    const index = state.images.findIndex(item => item.id === payload.id);
    Vue.set(state.images, index, payload);
  },
  [DELETE_IMAGE](state, payload) {
    state.images.map((image, index) => {
      if (image.id == payload.id) {
        state.images.splice(index, 1);
      }
    })
  },
  [SET_IMAGE_PAGE](state, payload) {
    state.page = payload;
  },
  [SET_IMAGE_TOTAL_PAGES](state, payload) {
    state.total_pages = payload;
  },
  [SET_IMAGE_SEARCH](state, payload) {
    state.search = payload;
  },
  [SET_IMAGE_STATUS](state, payload) {
    state.status = payload;
  },
  [SET_IMAGE_TYPE](state, payload) {
    state.type = payload;
  },
  [SET_IMAGE_TRANSPORT_TYPES](state, payload) {
    state.transport_types = payload;
  },
  [SET_IMAGE_CARGO_TYPES](state, payload) {
    state.cargo_types = payload;
  },
  [SET_IMAGE_TAGS](state, payload) {
    state.tags = payload;
  },
};

const actions = {
  async fetchImages({
    commit, state
  }, params = {}) {
    try {
      // filters
      if (!params.per_page) params.per_page = state.per_page;
      if (!params.page) params.page = state.page;
      if (!params.search) params.search = state.search ? state.search : void 0 ;
      if (!params.status) params.status = state.status;
      if (!params.type) params.type = state.type;
      if (!params.transport_types) params.transport_types = state.transport_types.length > 0 ? state.transport_types : void 0;
      if (!params.cargo_types) params.cargo_types = state.cargo_types.length > 0 ? state.cargo_types : void 0;
      if (!params.tags) params.tags = state.tags.length > 0 ? state.tags : void 0;

      const response = await fetchImages(params);

      if (!response.success || !response.data) {
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t load images.')
      }

      commit(SET_IMAGE_TOTAL_PAGES, Math.ceil(response.total / params.per_page));
      commit(FETCH_IMAGES, response.data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },

  async uloadImages({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let response = await uloadImages(payload);

      if (!response.success || !response.data) {
        console.log(response);
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t upload images.')
      }

      commit(CREATE_IMAGE, response.data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
      // return response.data;
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  async editImage({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await editImage(payload);
      commit(EDIT_IMAGE, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  async deleteImage({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await deleteImage(payload);
      commit(DELETE_IMAGE, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },
  // --
  setPage({
    commit
  }, page) {
    commit(SET_IMAGE_PAGE, page);
  },
  setSearch({
    commit
  }, search) {
    commit(SET_IMAGE_SEARCH, search);
  },
  setStatus({
    commit
  }, status) {
    commit(SET_IMAGE_STATUS, status);
  },
  setType({
    commit
  }, type) {
    commit(SET_IMAGE_TYPE, type);
  },
  setTransportTypes({
    commit
  }, transport_types) {
    commit(SET_IMAGE_TRANSPORT_TYPES, transport_types);
  },
  setCargoTypes({
    commit
  }, cargo_types) {
    commit(SET_IMAGE_CARGO_TYPES, cargo_types);
  },
  setTags({
    commit
  }, tags) {
    commit(SET_IMAGE_TAGS, tags);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
