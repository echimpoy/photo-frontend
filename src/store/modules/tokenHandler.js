import { getToken, setToken, removeToken } from '@/utils/auth';
import { SET_TOKEN, REMOVE_TOKEN } from '@/store/mutation-types';

const state = {
  token: getToken() || '',
};

const getters = {
  token: state => state.token,
  isAuth: state => !!state.token
};

const actions = {};

const mutations = {
  [SET_TOKEN](state, payload) {
    const token = `${payload}`;
    setToken(token);
    state.token = payload ? token : '';
  },
  [REMOVE_TOKEN](state, payload) {
    removeToken();
    state.token = payload;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};