import request from '@/utils/request';

export function login(data) {
  return request({
    url: '/auth/login',
    method: 'POST',
    data : {
      email: data.login,
      password: data.password,
    }
  });
}

export function updateProfile(data) {
  return request({
    url: '/user/update-profile',
    method: 'POST',
    data
  });
}

export function fetchUsers( params ) {
  return request({
    url: '/user',
    method: 'GET',
    params
  });
}

export function createUser(data) {
  return request({
    url: '/user',
    method: 'POST',
    data
  });
}

export function editUser(data) {
  return request({
    url: '/user/' + data.id,
    method: 'PUT',
    data
  });
}

export function deleteUser(data) {
  return request({
    url: '/user/' + data.id,
    method: 'DELETE'
  });
}
