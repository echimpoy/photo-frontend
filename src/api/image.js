import request from '@/utils/request';

export function fetchImages( params ) {
  return request({
    url: '/image',
    method: 'GET',
    params
  });
}

export function uloadImages(data) {
  return request({
    url: '/image',
    method: 'POST',
    data
  });
}

export function editImage(data) {
  return request({
    url: '/image/' + data.id,
    method: 'PUT',
    data
  });
}

export function deleteImage(data) {
  return request({
    url: '/image/' + data.id,
    method: 'DELETE'
  });
}
